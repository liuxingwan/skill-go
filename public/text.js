const promise = new Promise((resolve, reject) => {
  console.log(1)
  setTimeout(() => {
    console.log('timerStart')
    resolve('success')
    console.log('timerEnd')
  }, 0)
  console.log(2)
})

promise.then((res) => { console.log(res) }); console.log(4)

/** 首先遇到Promise构造函数，会先执行里面的内容，打印1；
遇到定时器steTimeout，它是一个宏任务，放入宏任务队列；
继续向下执行，打印出2；
由于Promise的状态此时还是pending，所以promise.then先不执行；
继续执行下面的同步任务，打印出4；
此时微任务队列没有任务，继续执行下一轮宏任务，执行steTimeout；
首先执行timerStart，然后遇到了resolve，将promise的状态改为resolved且保存结果并将之前的promise.then推入微任务队列，再执行timerEnd；
执行完这个宏任务，就去执行微任务promise.then，打印出resolve的结果
*/
