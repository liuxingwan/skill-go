const path = require('path')

function resolve(dir) {
  return path.join(__dirname, dir)
}
module.exports = {
  devServer: {
    host: '0.0.0.0',
    port: 4000,
    proxy: {
      // detail: https://cli.vuejs.org/config/#devserver-proxy
      [ process.env.VUE_APP_BASE_API ]: {
        // target: `http://47.97.207.154:8080`,
        target: `ws://47.96.122.14/skillgo`,
        changeOrigin: true,
        pathRewrite: {
          '^': ''
        }
      }
    },
    disableHostCheck: true
  },
  chainWebpack: (config) => {
    config.resolve.alias // 添加别名
      .set('@', resolve('src'))
    // 发布时移除console
    config.optimization.minimizer('terser').tap((args) => {
      const compress = args[ 0 ].terserOptions.compress

      compress.drop_console = true
      compress.pure_funcs = [
        '__f__', // App 平台 vue 移除日志代码
      ]
      return args
    })
  }
}
