export const FEEDBACK_TYPES = [ // 反馈类型
  { label: '平台BUG', value: 0 },
  { label: '平台体验修改建议', value: 1 },
  { label: '劣质项目', value: 2 },
  { label: '订单缺失/购买错误', value: 3 },
]
