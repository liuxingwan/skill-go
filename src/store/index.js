//登陆

import Vue from 'vue'
import Vuex from 'vuex'
// 引入腾讯地图jssdk文件
import  QQMapWX from "@/assets/js/qqmap-wx-jssdk.min.js"
Vue.use(Vuex)
export default new Vuex.Store ({
	state: {
		// 默认城市
		city: '北京市'
	},
	mutations:{
		newCityFun (state, newCity) {
			state.city = newCity
			console.log(state.city)
		}
	},
	actions: {
		getCity(context) {
			// 向用户发起授权请求，弹框提示
			uni.authorize({
			    // 获取用户定位信息
 				scope: "scope.userLocation",
 				// 用户同意授权执行
				success(){
					// 引入腾讯地图api
					// 实例化API核心类
					let qqmapsdk = new QQMapWX({
					     // 填写自己的Key值，这个值是与AppID绑定的
					     key: 'GFHBZ-JU3CD-NEJ4S-PT3J2-JGFH6-7DBOX'
					 });
					//获取位置信息
					uni.getLocation({
					    type: 'gcj02',
					    success: function (res) {
					        console.log('当前位置的经度：' + res.longitude)
					        console.log('当前位置的纬度：' + res.latitude)
							// 逆地址解析方法
							qqmapsdk.reverseGeocoder({
								location: {
									latitude: res.latitude,
									longitude: res.longitude
								},
								success(res) {
									var newCity = ''
									console.log(res)
									// 取到用户的定位城市，赋值传递出去
									newCity = res.result.address_component.city
									context.commit('newCityFun', newCity)
								}
							})	
					    }
					})
				},
				// 若用户不同意授权，弹框提示
				fail(res){
					uni.showToast({
						icon :"none",
					    title: '注意：需要获取您的定位授权,否则部分功能将无法使用',
					    duration: 2000
					})
				}
			})
		}
	}
})




