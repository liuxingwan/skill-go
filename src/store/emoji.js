export const emojis = [
  {
    id: 1,
    value: "[微笑]",
    url: "https://skillgo.cn/image/expression/emojipedia-b1.png",
  },
  {
    id: 2,
    value: "[可爱]",
    url: "https://skillgo.cn/image/expression/emojipedia-b02.png",
  },
  {
    id: 3,
    value: "[太开心]",
    url: "https://skillgo.cn/image/expression/emojipedia-b03.png",
  },
  {
    id: 4,
    value: "[鼓掌]",
    url: "https://skillgo.cn/image/expression/emojipedia-b04.png",
  },
  {
    id: 5,
    value: "[嘻嘻]",
    url: "https://skillgo.cn/image/expression/emojipedia-b05.png",
  },
  {
    id: 6,
    value: "[哈哈]",
    url: "https://skillgo.cn/image/expression/emojipedia-b06.png",
  },
  {
    id: 7,
    value: "[笑cry]",
    url: "https://skillgo.cn/image/expression/emojipedia-b07.png",
  },
  {
    id: 8,
    value: "[挤眼]",
    url: "https://skillgo.cn/image/expression/emojipedia-b08.png",
  },
  {
    id: 9,
    value: "[馋嘴]",
    url: "https://skillgo.cn/image/expression/emojipedia-b09.png",
  },
  {
    id: 10,
    value: "[黑线]",
    url: "https://skillgo.cn/image/expression/emojipedia-b10.png",
  },
  {
    id: 11,
    value: "[汗]",
    url: "https://skillgo.cn/image/expression/emojipedia-b11.png",
  },
  {
    id: 12,
    value: "[挖鼻]",
    url: "https://skillgo.cn/image/expression/emojipedia-b12.png",
  },
  {
    id: 13,
    value: "[哼]",
    url: "https://skillgo.cn/image/expression/emojipedia-c01.png",
  },
  {
    id: 14,
    value: "[怒]",
    url: "https://skillgo.cn/image/expression/emojipedia-c02.png",
  },
  {
    id: 15,
    value: "[委屈]",
    url: "https://skillgo.cn/image/expression/emojipedia-c03.png",
  },
  {
    id: 16,
    value: "[可怜]",
    url: "https://skillgo.cn/image/expression/emojipedia-c04.png",
  }, 
  {
    id: 17,
    value: "[失望]",
    url: "https://skillgo.cn/image/expression/emojipedia-c05.png",
  },
  {
    id: 18,
    value: "[悲伤]",
    url: "https://skillgo.cn/image/expression/emojipedia-c06.png",
  },
  {
    id: 19,
    value: "[泪]",
    url: "https://skillgo.cn/image/expression/emojipedia-c07.png",
  },
  {
    id: 20,
    value: "[允悲]",
    url: "https://skillgo.cn/image/expression/emojipedia-c08.png",
  },
  {
    id: 21,
    value: "[害羞]",
    url: "https://skillgo.cn/image/expression/emojipedia-c09.png",
  },
  {
    id: 22,
    value: "[污]",
    url: "https://skillgo.cn/image/expression/emojipedia-c10.png",
  },
  {
    id: 23,
    value: "[爱你]",
    url: "https://skillgo.cn/image/expression/emojipedia-c11.png",
  },
  {
    id: 24,
    value: "[亲亲]",
    url: "https://skillgo.cn/image/expression/emojipedia-c12.png",
  },
  {
    id: 25,
    value: "[色]",
    url: "https://skillgo.cn/image/expression/emojipedia-d01.png",
  },
  {
    id: 26,
    value: "[憧憬]",
    url: "https://skillgo.cn/image/expression/emojipedia-d02.png",
  },
  {
    id: 27,
    value: "[舔屏]",
    url: "https://skillgo.cn/image/expression/emojipedia-d03.png",
  },
  {
    id: 28,
    value: "[坏笑]",
    url: "https://skillgo.cn/image/expression/emojipedia-d04.png",
  },
  {
    id: 29,
    value: "[阴脸]",
    url: "https://skillgo.cn/image/expression/emojipedia-d05.png",
  },
  {
    id: 30,
    value: "[笑而不语]",
    url: "https://skillgo.cn/image/expression/emojipedia-d06.png",
  },
  {
    id: 31,
    value: "[偷笑]",
    url: "https://skillgo.cn/image/expression/emojipedia-d07.png",
  },
  {
    id: 32,
    value: "[酷]",
    url: "https://skillgo.cn/image/expression/emojipedia-d08.png",
  },
  {
    id: 33,
    value: "[并不简单]",
    url: "https://skillgo.cn/image/expression/emojipedia-d09.png",
  },
  {
    id: 34,
    value: "[思考]",
    url: "https://skillgo.cn/image/expression/emojipedia-d10.png",
  },
  {
    id: 35,
    value: "[疑问]",
    url: "https://skillgo.cn/image/expression/emojipedia-d11.png",
  },
  {
    id: 36,
    value: "[费解]",
    url: "https://skillgo.cn/image/expression/emojipedia-d12.png",
  },
  {
    id: 37,
    value: "[晕]",
    url: "https://skillgo.cn/image/expression/emojipedia-e01.png",
  },
  {
    id: 38,
    value: "[衰]",
    url: "https://skillgo.cn/image/expression/emojipedia-e02.png",
  },
  {
    id: 39,
    value: "[骷髅]",
    url: "https://skillgo.cn/image/expression/emojipedia-e03.png",
  },
  {
    id: 40,
    value: "[嘘]",
    url: "https://skillgo.cn/image/expression/emojipedia-e04.png",
  },
  {
    id: 41,
    value: "[闭嘴]",
    url: "https://skillgo.cn/image/expression/emojipedia-e05.png",
  },
  {
    id: 42,
    value: "[傻眼]",
    url: "https://skillgo.cn/image/expression/emojipedia-e06.png",
  },
  {
    id: 43,
    value: "[吃惊]",
    url: "https://skillgo.cn/image/expression/emojipedia-e07.png",
  },
  {
    id: 44,
    value: "[吐]",
    url: "https://skillgo.cn/image/expression/emojipedia-e08.png",
  },
  {
    id: 45,
    value: "[感冒]",
    url: "https://skillgo.cn/image/expression/emojipedia-e09.png",
  },
  {
    id: 46,
    value: "[生病]",
    url: "https://skillgo.cn/image/expression/emojipedia-e10.png",
  },
  {
    id: 47,
    value: "[拜拜]",
    url: "https://skillgo.cn/image/expression/emojipedia-e11.png",
  },
  {
    id: 48,
    value: "[鄙视]",
    url: "https://skillgo.cn/image/expression/emojipedia-e12.png",
  },
  {
    id: 49,
    value: "[白眼]",
    url: "https://skillgo.cn/image/expression/emojipedia-f01.png",
  },
  {
    id: 50,
    value: "[左哼哼]",
    url: "https://skillgo.cn/image/expression/emojipedia-f02.png",
  },
  {
    id: 51,
    value: "[右哼哼]",
    url: "https://skillgo.cn/image/expression/emojipedia-f03.png",
  },
  {
    id: 52,
    value: "[抓狂]",
    url: "https://skillgo.cn/image/expression/emojipedia-f04.png",
  },
  {
    id: 53,
    value: "[怒骂]",
    url: "https://skillgo.cn/image/expression/emojipedia-f05.png",
  },
  {
    id: 54,
    value: "[打脸]",
    url: "https://skillgo.cn/image/expression/emojipedia-f06.png",
  },
  {
    id: 55,
    value: "[顶]",
    url: "https://skillgo.cn/image/expression/emojipedia-f07.png",
  },
  {
    id: 56,
    value: "[互粉]",
    url: "https://skillgo.cn/image/expression/emojipedia-f08.png",
  },
  {
    id: 57,
    value: "[钱]",
    url: "https://skillgo.cn/image/expression/emojipedia-f09.png",
  },
  {
    id: 58,
    value: "[哈欠]",
    url: "https://skillgo.cn/image/expression/emojipedia-f10.png",
  },
  {
    id: 59,
    value: "[困]",
    url: "https://skillgo.cn/image/expression/emojipedia-f11.png",
  },
  {
    id: 60,
    value: "[睡]",
    url: "https://skillgo.cn/image/expression/emojipedia-f12.png",
  },
  {
    id: 61,
    value: "[求饶]",
    url: "https://skillgo.cn/image/expression/emojipedia-g1.png",
  },
  {
    id: 62,
    value: "[吃瓜]",
    url: "https://skillgo.cn/image/expression/emojipedia-g2.png",
  },
  {
    id: 63,
    value: "[打call]",
    url: "https://skillgo.cn/image/expression/emojipedia-g3.png",
  },
  {
    id: 64,
    value: "[awsl]",
    url: "https://skillgo.cn/image/expression/emojipedia-g4.png",
  },
  {
    id: 65,
    value: "[裂开]",
    url: "https://skillgo.cn/image/expression/emojipedia-g05.png",
  },
  {
    id: 66,
    value: "[牛年大吉]",
    url: "https://skillgo.cn/image/expression/emojipedia-g06.png",
  },
  {
    id: 67,
    value: "[牛大发]",
    url: "https://skillgo.cn/image/expression/emojipedia-g07.png",
  },
  {
    id: 68,
    value: "[牛哞哞]",
    url: "https://skillgo.cn/image/expression/emojipedia-g08.png",
  },
  {
    id: 69,
    value: "[doge]",
    url: "https://skillgo.cn/image/expression/emojipedia-g09.png",
  },
  {
    id: 70,
    value: "[二哈]",
    url: "https://skillgo.cn/image/expression/emojipedia-g10.png",
  },
  {
    id: 71,
    value: "[喵喵]",
    url: "https://skillgo.cn/image/expression/emojipedia-g11.png",
  },
  {
    id: 72,
    value: "[酸]",
    url: "https://skillgo.cn/image/expression/emojipedia-g12.png",
  },
  {
    id: 73,
    value: "[心]",
    url: "https://skillgo.cn/image/expression/emojipedia-j01.png",
  },
  {
    id: 74,
    value: "[伤心]",
    url: "https://skillgo.cn/image/expression/emojipedia-j02.png",
  },
  {
    id: 75,
    value: "[握手]",
    url: "https://skillgo.cn/image/expression/emojipedia-j05.png",
  },
  {
    id: 76,
    value: "[赞]",
    url: "https://skillgo.cn/image/expression/emojipedia-j06.png",
  },
  {
    id: 77,
    value: "[good]",
    url: "https://skillgo.cn/image/expression/emojipedia-j7.png",
  },
  {
    id: 78,
    value: "[NO]",
    url: "https://skillgo.cn/image/expression/emojipedia-j09.png",
  },
  {
    id: 79,
    value: "[耶]",
    url: "https://skillgo.cn/image/expression/emojipedia-j10.png",
  },
  {
    id: 80,
    value: "[ok]",
    url: "https://skillgo.cn/image/expression/emojipedia-j12.png",
  },
  {
    id: 81,
    value: "[作揖]",
    url: "https://skillgo.cn/image/expression/emojipedia-k03.png",
  },
  {
    id: 82,
    value: "[熊猫]",
    url: "https://skillgo.cn/image/expression/emojipedia-k05.png",
  },
  {
    id: 83,
    value: "[兔子]",
    url: "https://skillgo.cn/image/expression/emojipedia-k06.png",
  },
  {
    id: 84,
    value: "[猪头]",
    url: "https://skillgo.cn/image/expression/emojipedia-k07.png",
  },
  {
    id: 85,
    value: "[草泥马]",
    url: "https://skillgo.cn/image/expression/emojipedia-k08.png",
  },
  {
    id: 86,
    value: "[奥特曼]",
    url: "https://skillgo.cn/image/expression/emojipedia-k09.png",
  },
  {
    id: 87,
    value: "[太阳]",
    url: "https://skillgo.cn/image/expression/emojipedia-k10.png",
  },
  {
    id: 88,
    value: "[月亮]",
    url: "https://skillgo.cn/image/expression/emojipedia-k11.png",
  },
  {
    id: 89,
    value: "[锦鲤]",
    url: "https://skillgo.cn/image/expression/emojipedia-q05.png",
  },
  {
    id: 90,
    value: "[抱抱]",
    url: "https://skillgo.cn/image/expression/h02.png",
  },
  {
    id: 91,
    value: "[摊手]",
    url: "https://skillgo.cn/image/expression/h03.png",
  },
  {
    id: 92,
    value: "[跪了]",
    url: "https://skillgo.cn/image/expression/h04.png",
  },
  {
    id: 93,
    value: "[鲜花]",
    url: "https://skillgo.cn/image/expression/h05.png",
  },
  {
    id: 94,
    value: "[红灯笼]",
    url: "https://skillgo.cn/image/expression/h06.png",
  },
]