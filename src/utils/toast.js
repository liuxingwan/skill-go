/* 提示 */

export const toast = {
  /**
   * 纯文字提示
   * @param {String} title 提示文字
   * @param {Boolean} mask 是否显示蒙层
   * @param {Number} duration 持续时间
   */
  msg: function(title, mask = false, duration = 1500) {
    if (title) {
      uni.showToast({
        title,
        mask,
        duration,
        icon: 'none'
      })
    }
  },
  success: function(title, mask = true, duration = 1500) {
    uni.showToast({
      title,
      mask,
      duration
    })
  },

  error: function(title, mask = true, duration = 1500) {
    uni.showToast({
      title,
      image: '/static/imgs/close.png',
      mask,
      duration
    })
  }
}
export function Confirm(content, success, cancel) {
  uni.showModal({
    title: '提示',
    content,
    confirmColor: '#f8b86b',
    success: (res) => {
      if (res.confirm && success) {
        success()
      } else if (res.cancel && cancel) {
        cancel
      }
    }
  })
}
