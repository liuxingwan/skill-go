import { getUserInfo } from '@/store/user.js'

/**
 * 获取分享给好友路径
 * @param {String} title
 * @param {String} type
 * @param {String} id
 */
export function getShareAppData(desc = '', title = 'Charge元电荷') {
  const inviteUserId = getUserInfo().userInfo.userId

  let path = computeStartPath()

  /* 加入邀请人id*/
  if (path.indexOf('?') > 1) {
    path += `&inviteUserId=${ inviteUserId }`
  } else {
    path += `?inviteUserId=${ inviteUserId }`
  }
  console.log(path)

  return {
    title,
    desc,
    path,
  }
}
/**
 * 计算当前路径的scheme
 */
 export function getCurrentScheme() {
  // eslint-disable-next-line no-undef
  let currentPath = getCurrentPages()

  currentPath = currentPath[ currentPath.length - 1 ]
  const route = `/${ currentPath.route }`

  let scheme

  for (const key in SCHEME) {
    if (SCHEME[ key ].path === route) {
      scheme = key
      break
    }
  }
  return scheme ? qsScheme({ scheme, ...currentPath.options }) : 'home'
}

/**
 * 根据当前路径计算启动路径, 必须设置scheme，否则直接跳入口页
 */
export function computeStartPath() {
  const scheme = getCurrentScheme()

  return scheme ? `pages/Entry?scheme=${ scheme }` : 'pages/Entry'
}
