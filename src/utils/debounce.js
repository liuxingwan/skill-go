export function debounce(fn, delay) {
  let timer = null

  return function() {
    clearTimeout(timer)
    timer = setTimeout(() => {
      fn.apply(this, arguments)
    }, delay)
  }
}

/** websocket过滤器 */
export function StringToArray(str) {

}
