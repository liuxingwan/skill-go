import Vue from 'vue'

import { getToken, setToken, removeToken } from '@/store/user'

function myRequest(url, method, data) {
  /* 配置请求头 */
  const token = getToken()
  const headers = {
    'Content-Type': 'application/json; charset=utf-8',
    'Token': token
  }
  // http://localhost:8088

  return new Promise((resolve, reject) => {
    uni.request({
      url: 'https://skillgo.cn/test' + url,
      method: method,
      data: data,
      header: headers,
      timeout: 10000,
      success(result) {
        resolve(result.data)
      },
      fail(err) {
        console.log(err.data)
        Vue.prototype.gToastError('服务器错误')
        reject(err)
      },
      complete(result) {
        /* 判断是否有新token,有则替换旧的token */
        if (result.header.Authorization) {
          console.log(result.header.Authorization)
          setToken(result.header.Authorization)
        }
      }
    })
  })
}
export default myRequest
