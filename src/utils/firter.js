export function getPrice(str) {
  const arr = str[ 0 ].split('-')

  if (arr.length >= 2) {
    return arr.map(Number)
  } else {
    return [parseInt(arr[ 0 ]), 1000000000]
  }
}

export function preProcessData(formData) {
  /* 删除空值 */
  Object.keys(formData).forEach(item => {
    if (isEmpty(formData[ item ])) {
      delete formData[ item ]
    }
  })
  return formData
}

function isEmpty(obj) {
  if (typeof obj === 'undefined' || obj === null || obj === '') {
    return true
  } else {
    return false
  }
}
/**
 * 时间格式转换
 */
/**
 * @date:日期时间戳    获取当前时间戳  Date.parse(new Date())
 * @format:需要转换的格式：yyyy-MM-dd   yyyy-MM-dd hh:mm:ss
 */
export function dateFormat(date, format) {
  if (typeof date === 'string') {
    return formatDate(new Date(parseInt(date)), format)
  } else {
    return formatDate(new Date(date), format)
  }
}

function formatDate(date, format = 'yyyy-MM-dd') {
  let v = ''

  const year = date.getFullYear()

  const month = date.getMonth() + 1

  const day = date.getDate()

  const hour = date.getHours()

  const minute = date.getMinutes()

  const second = date.getSeconds()

  const weekDay = date.getDay()

  const ms = date.getMilliseconds()

  let weekDayString = ''

  if (weekDay == 1) {
    weekDayString = '星期一'
  } else if (weekDay == 2) {
    weekDayString = '星期二'
  } else if (weekDay == 3) {
    weekDayString = '星期三'
  } else if (weekDay == 4) {
    weekDayString = '星期四'
  } else if (weekDay == 5) {
    weekDayString = '星期五'
  } else if (weekDay == 6) {
    weekDayString = '星期六'
  } else if (weekDay == 7) {
    weekDayString = '星期日'
  }
  v = format
  // Year
  v = v.replace(/yyyy/g, year)
  v = v.replace(/YYYY/g, year)
  v = v.replace(/yy/g, (year + '').substring(2, 4))
  v = v.replace(/YY/g, (year + '').substring(2, 4))
  // Month
  const monthStr = ('0' + month)

  v = v.replace(/MM/g, monthStr.substring(monthStr.length - 2))
  // Day
  const dayStr = ('0' + day)

  v = v.replace(/dd/g, dayStr.substring(dayStr.length - 2))
  // hour
  const hourStr = ('0' + hour)

  v = v.replace(/HH/g, hourStr.substring(hourStr.length - 2))
  v = v.replace(/hh/g, hourStr.substring(hourStr.length - 2))
  // minute
  const minuteStr = ('0' + minute)

  v = v.replace(/mm/g, minuteStr.substring(minuteStr.length - 2))
  // Millisecond
  v = v.replace(/sss/g, ms)
  v = v.replace(/SSS/g, ms)
  // second
  const secondStr = ('0' + second)

  v = v.replace(/ss/g, secondStr.substring(secondStr.length - 2))
  v = v.replace(/SS/g, secondStr.substring(secondStr.length - 2))
  // weekDay
  v = v.replace(/E/g, weekDayString)
  return v
}
