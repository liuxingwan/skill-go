
/* 小程序路由跳转 */
import Vue from 'vue'
import { toast } from './toast'

export function navigateToLogin(params, success) {
  uni.navigateTo({
    url: `/packages/User/Login/index`,
    success
  })
}
export function redirectToMyinfor(params, success) {
  uni.redirectsTo({
    url: `/pages/Myinfor/index `,
    success
  })
}
export function switchToMyinfor(params) {
  uni.switchTab({
    url: `/pages/Myinfor/index `
  })
}
export function relaunchToLogin(params, success) {
  uni.reLaunch({
    url: `/packages/User/Login/index `,
    success
  })
}
/**
 * 跳转登录页
 * @param {Funtion} successCb
 */
export function relaunchLogin(successCb) {
  uni.reLaunch({
    url: '/pages/User/Login',
    success: successCb
  })
}
/**
 * 跳转到主页并且关闭当前页面
 * @param {*} params
 * @param {*} successCb
 */
export function redirectApp(successCb) {
  uni.reLaunch({
    url: '/pages/User/Login',
    success: successCb
  })
}
/**
 * 跳转到确认购买页面
 */

export function redirectPurchase(successCb) {
  uni.reLaunch({
    url: '/packages/ChatPages/PurchaseConfirm/index',
    success: successCb
  })
}

/**
 * 跳转到确认购买页面，不关闭页面
 */

export function navigatePurchase(param) {
  uni.reLaunch({
    url: '/packages/ChatPages/PurchaseConfirm/index' + '?' + convertObj(param)
  })
}

/**
 * 把参数配置上路由
 */
function convertObj(data) {
  const _result = []

  for (var key in data) {
    const value = data[ key ]

    if (value == null) {
      continue
    } else if (value.constructor == Array) {
      value.forEach(function(_value) {
        _result.push(key + '=' + _value)
      })
    } else {
      _result.push(key + '=' + value)
    }
  }
  return _result.join('&')
}
