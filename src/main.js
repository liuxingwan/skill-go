import Vue from 'vue'
import App from './App'
import store from './store/index.js'

Vue.config.productionTip = false
/* 字体图标 */
import '@/styles/iconfont.css'

App.mpType = 'app'

/* UI库 */
import uView from '@/uni_modules/uview-ui'
Vue.use(uView)
/* UI库
import uView2 from '@/uview-ui'
Vue.use(uView2)*/
/* 注册全局方法 */
require('./utils/config/registerFunction.js')

const app = new Vue({
  ...App
})

app.$mount()
