export const compuslist = [
  { compusid: 1, compus: '朝晖校区' },
  { compusid: 2, compus: '屏峰校区' },
  { compusid: 3, compus: '莫干山校区' }
]

export const GoodType = [
  { label: '电瓶车', type: 1 },
  { label: '课本', type: 2 }
]

/**
 * 图片的基本url
 */
export const baseUrls = {
  avater: 'https://skillgo.cn/chargePics/'
}

/**
 * 交易映射
 */
export const orderStatus = {
  '-1': '交易失败',
  '1': '待发货',
  '2': '待收货',
  '3': '双方待评价',
  '4': '买家待评价',
  '5': '商家待评价'

}
