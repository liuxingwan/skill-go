// 充电桩的集中情况下的icon
export const locatiosIcon = {
  '1': 'https://skillgo.cn/image/charge/1.png',
  '2': 'https://skillgo.cn/image/charge/2.png',
  '3': 'https://skillgo.cn/image/charge/3.png',
  '4': 'https://skillgo.cn/image/charge/4.png',
  '5': 'https://skillgo.cn/image/charge/5.png',
  '0': 'https://skillgo.cn/image/charge/0.png',
  'my': 'https://skillgo.cn/image/charge/my.png.png'
}
// 两套页面图片
export const pic = {
  '2': {
    'refash': 'https://skillgo.cn/image/Chargepic/chargeMap/refash.png',
    'changeNight': 'https://skillgo.cn/image/Chargepic/chargedark/night.png',
    'see': 'https://skillgo.cn/image/Chargepic/chargedark/see.png',
    'funAll': 'https://skillgo.cn/image/Chargepic/chargedark/single.png',
    'funsimple': 'https://skillgo.cn/image/Chargepic/chargedark/empety.png',
    'funsingle': 'https://skillgo.cn/image/Chargepic/chargedark/all.png',
    'location': 'https://skillgo.cn/image/Chargepic/chargedark/getLocation.png',
    'chargemap': 'https://skillgo.cn/image/Chargepic/chargedark/linght.png',
    'changeLocation': 'https://skillgo.cn/image/Chargepic/chargedark/location.png',
    'fouce': 'https://skillgo.cn/image/Chargepic/chargedark/fouces.png',
    'my': 'https://skillgo.cn/image/Chargepic/chargeMap/chargemy.png',
    'activeempety': 'https://skillgo.cn/image/Chargepic/chargedark/empetyActive.png',
    'activesingle': 'https://skillgo.cn/image/Chargepic/chargedark/singleActive.png',
    'activeAll': 'https://skillgo.cn/image/Chargepic/chargedark/allActive.png'
  }, '3':
{
  'refash': 'https://skillgo.cn/image/Chargepic/chargedark/nightrefrash.png',
  // 'refash':"https://skillgo.cn/image/Chargepic/chargeMap/refash.png",
  'changeNight': 'https://skillgo.cn/image/Chargepic/chargedark/sun.png',
  'see': 'https://skillgo.cn/image/Chargepic/chargedark/nightsew.png',

  'funAll': 'https://skillgo.cn/image/Chargepic/chargedark/nightsingle.png',
  'funsimple': 'https://skillgo.cn/image/Chargepic/chargedark/nightempety.png',
  'funsingle': 'https://skillgo.cn/image/Chargepic/chargedark/nightall.png',

  'location': 'https://skillgo.cn/image/Chargepic/chargedark/nightmy.png',
  'chargemap': 'https://skillgo.cn/image/Chargepic/chargedark/linght.png',
  'changeLocation': 'https://skillgo.cn/image/Chargepic/chargedark/location.png',
  'fouce': 'https://skillgo.cn/image/Chargepic/chargedark/fouces.png',
  'my': 'https://skillgo.cn/image/Chargepic/chargeMap/chargemy.png',
  'activeempety': 'https://skillgo.cn/image/Chargepic/chargedark/empetyActive.png',
  'activesingle': 'https://skillgo.cn/image/Chargepic/chargedark/singleActive.png',
  'activeAll': 'https://skillgo.cn/image/Chargepic/chargedark/allActive.png'
},
}
