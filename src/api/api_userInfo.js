import request from '../utils/request.js'
// 根据设备编号获取设备信息
exports.getequment = (plug) => request('/thirdparty/devices/' + plug, 'GET')
// 登录
exports.login = (data) => request('/login', 'POST', data)
// 查看设备的插座状态
// exports.allequiment=(data)=>request("/thirdparty/devices/"+data+"/plugs")
/** java登陆
export function login2(data){
	return request('/weixin/sessionId/'+data,"GET")
}
//获取数据
export function allequiment(data){
	return request("/weixin/getplugs","GET",data)
}*/

// 登陆获取token
export function login2(data) {
  return request('/sessionId', 'GET', data)
}
// 通过token等一系列数据实现
export function authlogin(data) {
  return request('/data/userInfo', 'post', data)
}
// 获取数据
export function allequiment(data) {
  return request('/weixin/getBylocationid', 'get', data)
}

// 修改用户的数据根据全量信息
export const updateUserinfor = (data) => request('/data/userInfo', 'put', data)

// 获取用户的所有信息
export const userinfo = (data) => request(`/data/userInfo/${ data }`, 'get')
// 获取用户的所有信息
export const updateUserinfo = (data) => request(`/data/userInfo`, 'PUT', data)

// 获取桩位的所有数据
export const getLocation = (data) => request('/data/emptyPlugLocation/all')

// 根据校区进行访问所有数据  /data/plugLocation
export const getAllbycapus = (data) => request('/data/plugLocation', data)

// 根据校区进行访问空数据
export const getemptybycapus = (data) => request('/data/emptyPlugLocation', data)

//
export const getStreambycapus = (data) => request('/data/streamlinePlugsLocation', data)

