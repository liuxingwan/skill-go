import request from '../utils/request.js'

// 获取校区
export function getSchool() {
  return request('/schoolInfo/school', 'get')
}
// 获取校区
export function getcollege(data) {
  return request('/schoolInfo/college', 'get', data)
}
// 获取校区
export function getCmpus(data) {
  return request('/schoolInfo/campus', 'get', data)
}
// 添加用户
export function login(data) {
  return request('/data/userInfo', 'post', data)
}
// 修改用户的数据根据全量信息
export const updateUserinfor = (data) => request('/data/userInfo', 'put', data)

// 根据ID获取用户信息
export const getUserById = (data) => request('/data/userInfo/id', 'get', data)

/**
 * 用户登陆
 */
export function LoginByCode(data) {
  return request('/login/openId', 'get', data)
}
