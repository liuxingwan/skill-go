import request from '@/utils/request.js'

// 进入聊天页面
export function InChat(data) {
  return request('/message/enter', 'get', data)
}

/**
 * @param{String} 分页获取聊天记录?senderId=111&receiveId=222&page=1&size=20
 */
export function InChatBypage(data) {
  return request('/message/id', 'get', data)
}

/**
 * 离开聊天界面
 * @param{senderId=222}
 * @param{receiveId=111}
 */
export function leaveChat(data) {
  return request('/message/exit', 'get', data)
}

/**
 * 获取消息列表 /message/messageList
 */
export function getmessageList(data) {
  return request('/message/messageList', 'get', data)
}

/**
 * 私聊图片上传
 *
 */
export function uploadPic(data) {
  return request('/shopping/uploadPicture', 'post', data)
}

/**
 * 点击立即购买
 * /message/buyImmediately
 */
export function buyImmediately(data) {
  return request('/message/buyImmediately', 'post', data)
}

/**
 * 消息添加
 */
export function imMessage(data) {
  return request('/message/imMessage', 'post', data)
}

/**
 * 买房确认
 */
export function confirmBuy(data) {
  return request(`/message/buyerOption?id=${ data.id }&option=${ data.option }`, 'post', data)
}
/**
 * 卖房确认
 */
export function confirmSell(data) {
  return request(`/message/sellerOption?id=${ data.id }&option=${ data.option }`, 'post')
}
