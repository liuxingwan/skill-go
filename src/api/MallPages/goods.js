import request from '@/utils/request.js'
/**
 * 根据id获取商品信息
 */
export function getGoodsInfo(id) {
  return request('/shopping/commodity/id', 'GET', id)
}

/**
 * 获取项目列表  时间排序
 * ?data=本&sort=ASC&page=1&size=2&type=2
 */
export function getGoodsBy(url, data) {
  return request(`/shopping/commodity/orderBy/${ url }`, 'GET', data)
}

/**
 * 获取电瓶车属性
 * @param {*} data
 * @returns
 */
export function getBatteryCar(data) {
  return request(`/shopping/commodityAttribute/commodityId`, 'GET', data)
}

/**
 * 获取项目列表  时间排序
 * ?data=本&sort=ASC&page=1&size=2&type=2
 */
export function getPrice(data) {
  return request('/shopping/commodity/orderBy/price', 'GET', data)
}

/**
 * 获取项目列表  时间排序
 * ?data=本&sort=ASC&page=1&size=2&type=2
 */
export function getGoodsBycomment(data) {
  return request('/shopping/commodity/orderBy/comment', 'GET', data)
}

/**
 * 获取项目列表  时间排序
 * ?data=本&sort=ASC&page=1&size=2&type=2
 */
export function getGoodsByBrowse(data) {
  return request('/shopping/commodity/orderBy/collectionAndBrowse', 'GET', data)
}
/**
 * 添加数据
 */
export function addGoodsData(data) {
  return request('/shopping/commodity', 'POST', data)
}

/**
 * 发布商品
 */
export function postGoodsPublish(data) {
  return request('/shopping/commodityAttribute/list', 'POST', data)
}
// http://skillgo.cn/test/shopping/commodity
/**
 * 获取所有分类
 */
export function getAllType(data) {
  return request('/shopping/commodityType/all', 'GET', data)
}
/**
 * 根据分类获取属性和值
 */
export function getAttributeByTypeId(data) {
  return request('/shopping/Attribute/belongingType', 'GET', data)
}
/**
 * 根据用户ID获取商品信息
 */
export function getGoodsByUserId(data) {
  return request('/shopping/commodity/publisherId', 'GET', data)
}
/**
 * 根据卖家id获取订单
 */
export function getOrderBySellerId(data) {
  return request('/shopping/transaction/sellerId', 'GET', data)
}
/**
 * 根据id获取个人信息
 */
export function getUserInfoById(data) {
  return request('/data/userInfo/id', 'GET', data)
}
/**
 * 判断当前商品是否收藏
 */
export function getIsCollection(data) {
  return request('/shopping/collection/ifCollection', 'POST', data)
}

/**
 * 收藏当前商品
 */
export function collectionGoodById(data) {
  return request('/shopping/collection', 'POST', data)
}
/**
 * 取消收藏当前商品
 */
export function deleteCollection(data) {
  return request('/shopping/collection', 'DELETE', data)
}
/**
 * 查询自己是否关注当前用户
 */
export function getIsFollowUser(data) {
  return request('/shopping/follow/ifFollowed', 'GET', data)
}
/**
 * 关注当前用户
 */
export function postfollowUser(data) {
  return request('/shopping/follow', 'POST', data)
}
/**
 * 取消关注当前用户
 */
export function deleteFollowUser(data) {
  return request('/shopping/follow', 'DELETE', data)
}
/**
 * 0元商品
 */
export function zeroGoods(data) {
  return request('/shopping/commodity/zero', 'GET', data)
}