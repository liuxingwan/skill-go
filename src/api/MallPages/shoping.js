import request from '@/utils/request.js'

/**
 * 根据id获取商品信息
 * @param{Number} 商品ID /shopping/transaction
 */
export function buyGood(id) {
  return request('/shopping/buy', 'POST', id)
}

/**
 * 根据id删除商品信息
 * @param{Number} 商品ID /shopping/transaction
 */
export function deleteGood(id) {
  return request('/shopping/transaction', 'DELETE', id)
}
