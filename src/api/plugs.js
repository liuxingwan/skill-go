import request from '../utils/request.js'

// 获取所有的充电桩
export function plugLocations() {
  return request('/data/plugLocation/all', 'get')
}
// 获取所有的空的充电桩
export function emptyPlugs() {
  return request('/data/emptyPlugLocation/all', 'get')
}
// 获取所有的充电桩
export function getBylocationid() {
  return request('/weixin/getBylocationid', 'get')
}
// 获取所有精简版本充电桩
export function streamlinePlugsLocation() {
  return request('/data/streamlinePlugsLocation/all', 'get')
}

// 根据校区获取所有的充电桩
export function plugsByCom(data) {
  return request('/data/plugLocation', 'get', data)
}
// 根据校区获取所有的空的充电桩
export function emptyPlugByCom(data) {
  return request('/data/emptyPlugLocation', 'get', data)
}
// 根据校区获取所有的空的井简版充电桩
export function streamlinePlugsByCom(data) {
  return request('/data/streamlinePlugsLocation', 'get', data)
}

// 获取桩位信息根据id
export function PlugsByid(data) {
  return request('/data/PlugsLocationByCode', 'get', data)
}
// 桩位
export function NumSub(data) {
  return request('/data/concernedNumAdd', 'put', data)
}
// 桩位
export function NumSubdec(data) {
  return request('/data/concernedNumSub', 'put', data)
}
