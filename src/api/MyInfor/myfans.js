import request from '@/utils/request.js'

/**
 * 获取用户
 */
export function followUserId(data) {
  return request('/shopping/follow/followUserId', 'GET', data)
}
/**
 * 获取用户关注
 * @param {Number} id
 * @returns
 */
export function beFollowUserId(data) {
  return request('/shopping/follow/beFollowUserId', 'GET', data)
}

/**
 * 取消改用户的关注
 */
export function cancelFollow(data) {
  return request('/shopping/follow', 'DELETE', data)
}

/**
 * 关注用户
 */
export function PostFollow(data) {
  return request('/shopping/follow', 'POST', data)
}
