import request from '@/utils/request.js'

/**
 * 获取用户的关注
 */
export function beFollowUserId(data) {
  return request('shopping/follow/beFollowUserId', 'GET', data)
}

/**
 * 取消改用户的关注
 */
export function cancel(data) {
  return request('shopping/follow/beFollowUserId', 'DELETE', data)
}
