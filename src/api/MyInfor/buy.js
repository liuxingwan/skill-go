import request from '@/utils/request.js'

/**
 * 取消订单
 */
export function closesShop(data) {
  return request('/shopping/close', 'GET', data)
}

/**
 * 收货
 */
export function receiveGood(data) {
  return request('/shopping/receive', 'GET', data)
}

/**
 * 取消收藏
 */
export function collectionDelete(data) {
  return request('/shopping/collection', 'DELETE', data)
}
