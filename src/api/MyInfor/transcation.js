import request from '@/utils/request.js'

// 订单详情
/**
 * 获取订单列表
 * @param{}
 */
export function getorderList(data) {
  return request('/shopping/transaction/buyerId', 'GET', data)
}

/**
 * 获取买家列表
 * @param{}
 */
export function getBuyList(data) {
  return request('/shopping/transaction/sellerId', 'GET', data)
}
/**
 * 订单的添加  下单
 * @param {Object} data
 */
export function collectionPost(data) {
  return request('/shopping/buy', 'POST', data)
}

/**
 * 取消收藏
 */
export function collectionDelete(data) {
  return request('shopping/collection', 'DELETE', data)
}

/**
 *
 * 买家对卖家进行评价
 */
export function buyerComment(data) {
  return request('/shopping/buyerComment', 'POST', data)
}

/**
 *
 * 卖家对买家进行评价
 */
export function sellerComment(data) {
  return request('/shopping/sellerComment', 'POST', data)
}

/**
 * 修改订单为有收货
 */
export function receive(data) {
  return request('/shopping/receive', 'GET', data)
}

/**
 * 删除本条数据
 */
export function Deletetransaction(data) {
  return request('/shopping/transaction', 'DELETE', data)
}
