import request from '@/utils/request.js'

/**
 * 根据用户ID获取用户浏览记录
*/
export function browseRecordsList(id) {
  return request('/shopping/browseRecords/userId', 'GET', id)
}

/**
 * 添加记录
 */
export function browseRecordsPost(data) {
  return request('/shopping/browseRecords', 'POST', data)
}

/**
 * 我商品的信息的删除
 */

export function cancelbrowseRecords(data) {
  return request('/shopping/browseRecords', 'DELETE', data)
}

