import request from '@/utils/request.js'

/**
 * 根据用户ID获取用户的发布信息
 */
export function publisherId(id) {
  return request('/shopping/commodity/publisherId', 'GET', id)
}
/**
 * 根据商品ID获取用户的发布信息
 */
export function GoodInforById(id) {
  return request('/shopping/commodity/id', 'GET', id)
}
/**
 * 我的发布的商品信息的发布
 */
export function commodityPost(data) {
  return request('/shopping/commodity', 'POST', data)
}

/**
 * 我商品的信息的更改
 */

export function commodityPut(data) {
  return request('/shopping/commodity', 'PUT', data)
}

/**
 * 我商品的信息的删除
 */

export function cancelcommodity(data) {
  return request('/shopping/commodity', 'DELETE', data)
}
