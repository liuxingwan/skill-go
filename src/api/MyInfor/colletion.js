import request from '@/utils/request.js'

// 收藏信息
/**
 * 获取收藏列表
 */
export function getCollection(data) {
  return request('/shopping/collection/userId', 'GET', data)
}

/**
 * 点击收藏按钮
 */
export function collectionPost(data) {
  return request('/shopping/collection', 'POST', data)
}

/**
 * 取消收藏
 */
export function collectionDelete(data) {
  return request('/shopping/collection', 'DELETE', data)
}
