module.exports = {
  "extends": [
    "stylelint-config-standard",                // css 标准规则
    "stylelint-config-recess-order",            // 排序
  ],
  "plugins": ["stylelint-scss"],                // sass配置
  "rules": {
    "at-rule-no-unknown": null,                 // 忽略sass @报错问题
    "scss/at-rule-no-unknown": true,
    "unit-no-unknown": [true, {                 // 忽略单位rpx
      "ignoreUnits": ['rpx']
    }],                    
    "selector-type-no-unknown": [true, {        // 小程序专属标签
      "ignoreTypes": [
        "page", 
        "navigator", 
        "view", 
        "text",
        "picker"
      ]
    }],
    "declaration-colon-space-after": "always",  // 总有空格
    "color-hex-length": "long",                 // 16进制长度
    "rule-empty-line-before": "always",         // 两个声明有空行
    "string-quotes": "single",                  // 单引号
  }
}